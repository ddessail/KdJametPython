#include <stdio.h>

/* Definition  des variables pour le reseau avec Rrs490/Rrs555 >=.85 */

/* nombre de couches cachees - hidden layer number */
#define rsup2_NC 1
/* nombre de donnees d entree - input data number*/
#define rsup2_NE 5
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define rsup2_NC1 10
/* nombre de donnees de sortie - nb output*/
#define rsup2_NS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define rsup2_NES 6

/* LUTs file names */
#define rsup2_LUT_POIDS "../LUTS/KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_555_log_Kd_lambda_merge_seuil_15_angle_ascii_6x1_hh_10_publi_250314.sn"
#define rsup2_LUT_MOY "../LUTS/Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_angle_publi_250314.dat"
#define rsup2_LUT_ECART "../LUTS/Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_angle_publi_250314.dat"


/* Definition  des variables pour le reseau avec Rrs490/Rrs555 <.85 */
/* nombre de couches cachees - hidden layer number */
#define rinf2_NC 1
/* nombre de donnees d entree - input data number*/
#define rinf2_NE 6
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define rinf2_NC1 10
/* nombre de donnees de sortie - nb output*/
#define rinf2_NS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define rinf2_NES 7
/* LUTs file names */
#define rinf2_LUT_POIDS "../LUTS/KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_log_Kd_lambda_merge_seuil_15_angle_ascii_6x1_hh_10_publi_250314.sn"
#define rinf2_LUT_MOY "../LUTS/Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_angle_publi_250314.dat"
#define rinf2_LUT_ECART "../LUTS/Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_angle_publi_250314.dat"

/* LUTs parameters definition */
float rsup2_b1[rsup2_NC1], rsup2_b2;
float rsup2_w1[rsup2_NE][rsup2_NC1], rsup2_w2[rsup2_NC1];
float rsup2_moy[rsup2_NES], rsup2_ecart[rsup2_NES];


float rinf2_b1[rinf2_NC1], rinf2_b2;
float rinf2_w1[rinf2_NE][rinf2_NC1], rinf2_w2[rinf2_NC1];
float rinf2_moy[rinf2_NES], rinf2_ecart[rinf2_NES];


