
 Python(2) version for Jamet's Kd 
 
- test_class_KdNN.py
 A small Test case application which use class_KdJamet.py
 Must be launch with three arguments, this way :
 $ LOG_KdNeuralNetwork_test LOG_KdNeuralNetwork_TestCase_Input.csv your_output.csv SensorID [idMERIS, idSEAWIFS, idMODIS]

 Result are in your_output.csv, which may match LOG_KdNeuralNetwork_TestCase_Output[Sensor]_Python.csv if everithing is OK !

 See comments in source code for more information.

 Contact for code implementation : david.dessailly@univ-littoral.fr

 Reference :
 Jamet, C., H. Loisel, and D. Dessailly (2012), Retrieval of the spectral diffuse
 attenuation coefficient Kd(lambda) in open and coastal ocean waters using a neural network inversion,
 J. Geophys. Res.,117, C10023, doi:10.1029/2012JC008076
