#include <stdio.h>

/* =======================================================================================================
 *  SeaWiFS 
 * ======================================================================================================= */
/* Definition  des variables pour le reseau avec Rrs490/Rrs555 > 1.5 */
/* nombre de couches cachees - hidden layer number */
#define rsupSW_NC 2
/* nombre de donnees d entree - input data number*/
#define rsupSW_NE 5
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define rsupSW_NC1 6
/* nombre de neurones de la deuxieme couche cachee */
#define rsupSW_NC2 4
/* nombre de donnees de sortie - nb output*/
#define rsupSW_NS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define rsupSW_NES 6

/* LUTs file names */
#define rsupSW_LUT_POIDS "KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_555_log_Kd_lambda_merge_seuil_15_angle_ascii_6x1_hh_6_4_publi_280314.sn"
#define rsupSW_LUT_MOY "Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_angle_publi_280314.dat"
#define rsupSW_LUT_ECART "Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_angle_publi_280314.dat"


/* Definition  des variables pour le reseau avec Rrs490/Rrs555 <= 1.5 */
/* nombre de couches cachees - hidden layer number */
#define rinfSW_NC 2
/* nombre de donnees d entree - input data number*/
#define rinfSW_NE 6
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define rinfSW_NC1 8
/* nombre de neurones de la deuxieme couche cachee */
#define rinfSW_NC2 5
/* nombre de donnees de sortie - nb output*/
#define rinfSW_NS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define rinfSW_NES 7

/* LUTs file names */
#define rinfSW_LUT_POIDS "KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_log_Kd_lambda_merge_seuil_15_angle_ascii_6x1_hh_8_5_publi_280314.sn"
#define rinfSW_LUT_MOY "Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_angle_publi_280314.dat"
#define rinfSW_LUT_ECART "Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_angle_publi_280314.dat"

/* LUTs parameters definition */
float rsupSW_b1[rsupSW_NC1], rsupSW_b2[rsupSW_NC2], rsupSW_b3;
float rsupSW_w1[rsupSW_NE][rsupSW_NC1], rsupSW_w2[rsupSW_NC1][rsupSW_NC2], rsupSW_w3[rsupSW_NC2];
float rsupSW_moy[rsupSW_NES], rsupSW_ecart[rsupSW_NES];

float rinfSW_b1[rinfSW_NC1], rinfSW_b2[rinfSW_NC2], rinfSW_b3;
float rinfSW_w1[rinfSW_NE][rinfSW_NC1], rinfSW_w2[rinfSW_NC1][rinfSW_NC2], rinfSW_w3[rinfSW_NC2];
float rinfSW_moy[rinfSW_NES], rinfSW_ecart[rinfSW_NES];


/* =======================================================================================================
 *  MODIS 
 * ======================================================================================================= */
/* Definition  des variables pour le reseau avec Rrs490/Rrs555 > 1.5 */
/* nombre de couches cachees - hidden layer number */
#define rsupMO_NC 2
/* nombre de donnees d entree - input data number*/
#define rsupMO_NE 5
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define rsupMO_NC1 5
/* nombre de neurones de la deuxieme couche cachee */
#define rsupMO_NC2 4
/* nombre de donnees de sortie - nb output*/
#define rsupMO_NS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define rsupMO_NES 6

/* LUTs file names */
#define rsupMO_LUT_POIDS "KdMODIS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_555_log_Kd_lambda_merge_seuil_15_angle_ascii_6x1_hh_5_4_publi_CSIRO.sn"
#define rsupMO_LUT_MOY "Moy_KdMODIS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_555_log_Kd_lambda_merge_seuil_15_ss_645_publi_sup_CSIRO.dat"
#define rsupMO_LUT_ECART "Ecart_KdMODIS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_555_log_Kd_lambda_merge_seuil_15_ss_645_publi_sup_CSIRO.dat"


/* Definition  des variables pour le reseau avec Rrs490/Rrs555 <= 1.5 */
/* nombre de couches cachees - hidden layer number */
#define rinfMO_NC 2
/* nombre de donnees d entree - input data number*/
#define rinfMO_NE 6
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define rinfMO_NC1 10
/* nombre de neurones de la deuxieme couche cachee */
#define rinfMO_NC2 7
/* nombre de donnees de sortie - nb output*/
#define rinfMO_NS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define rinfMO_NES 7

/* LUTs file names */
#define rinfMO_LUT_POIDS "KdMODIS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_log_Kd_lambda_merge_seuil_15_angle_ascii_6x1_hh_10_7_publi_CSIRO.sn"
#define rinfMO_LUT_MOY "Moy_KdMODIS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_ss_645_publi_inf_CSIRO.dat"
#define rinfMO_LUT_ECART "Ecart_KdMODIS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_ss_645_publi_inf_CSIRO.dat"

/* LUTs parameters definition */
float rsupMO_b1[rsupMO_NC1], rsupMO_b2[rsupMO_NC2], rsupMO_b3;
float rsupMO_w1[rsupMO_NE][rsupMO_NC1], rsupMO_w2[rsupMO_NC1][rsupMO_NC2], rsupMO_w3[rsupMO_NC2];
float rsupMO_moy[rsupMO_NES], rsupMO_ecart[rsupMO_NES];

float rinfMO_b1[rinfMO_NC1], rinfMO_b2[rinfMO_NC2], rinfMO_b3;
float rinfMO_w1[rinfMO_NE][rinfMO_NC1], rinfMO_w2[rinfMO_NC1][rinfMO_NC2], rinfMO_w3[rinfMO_NC2];
float rinfMO_moy[rinfMO_NES], rinfMO_ecart[rinfMO_NES];
