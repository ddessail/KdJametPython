#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include "neuron_switch2.h"

/* -------------------------------------------------------------------------------
 Lecture des differrentes Look Up Tables
 -------------------------------------------------------------------------------  */
void neuron_lect_LUTswitch2()
{
  FILE *fic;
  int i,j,poub;
  char *ligne=malloc(sizeof(char)*150);
  float fpoub;

  /* ----- LUTs for Rrs490/Rrs555 >= .85 ------ */
  if( (fic=fopen(rsup2_LUT_POIDS,"r")) == NULL) {perror(rsup2_LUT_POIDS); exit(-1);}
  fgets(ligne,150,fic);
  for(i=0; i<rsup2_NC1; i++)
    fscanf(fic,"%d %d %f",&poub,&poub,&rsup2_b1[i]);
  fscanf(fic,"%d %d %f",&poub,&poub,&rsup2_b2);
  for(j=0; j<rsup2_NE; j++){
   for(i=0; i<rsup2_NC1; i++)
     fscanf(fic,"%d %d %f",&poub,&poub,&rsup2_w1[j][i]);
  }
  for(j=0; j<rsup2_NC1; j++)
    fscanf(fic,"%d %d %f",&poub,&poub,&rsup2_w2[j]);
  fclose(fic);

  if( (fic=fopen(rsup2_LUT_MOY,"r")) == NULL) {perror(rsup2_LUT_MOY); exit(-1);}
  fscanf(fic,"%f",&fpoub);
  for(i=0; i<rsup2_NE-1; i++)
    fscanf(fic,"%f",&rsup2_moy[i]);
  fscanf(fic,"%f",&fpoub);
  fscanf(fic,"%f",&rsup2_moy[rsup2_NE-1]);
  fscanf(fic,"%f",&fpoub);
  fscanf(fic,"%f",&rsup2_moy[rsup2_NES-1]);
  fclose(fic);
  if( (fic=fopen(rsup2_LUT_ECART,"r")) == NULL) {perror(rsup2_LUT_ECART); exit(-1);}
  fscanf(fic,"%f",&fpoub);
  for(i=0; i<rsup2_NE-1; i++)
    fscanf(fic,"%f",&rsup2_ecart[i]);
  fscanf(fic,"%f",&fpoub);
  fscanf(fic,"%f",&rsup2_ecart[rsup2_NE-1]);
  fscanf(fic,"%f",&fpoub);
  fscanf(fic,"%f",&rsup2_ecart[rsup2_NES-1]);
  fclose(fic);


/* ----- LUTs for Rrs490/Rrs555 < .85 ------ */
  if( (fic=fopen(rinf2_LUT_POIDS,"r")) == NULL) {perror(rinf2_LUT_POIDS); exit(-1);}
  fgets(ligne,150,fic);
  for(i=0; i<rinf2_NC1; i++)
    fscanf(fic,"%d %d %f",&poub,&poub,&rinf2_b1[i]);
  fscanf(fic,"%d %d %f",&poub,&poub,&rinf2_b2);
  for(j=0; j<rinf2_NE; j++){
   for(i=0; i<rinf2_NC1; i++)
     fscanf(fic,"%d %d %f",&poub,&poub,&rinf2_w1[j][i]);
  }
  for(j=0; j<rinf2_NC1; j++)
    fscanf(fic,"%d %d %f",&poub,&poub,&rinf2_w2[j]);
  fclose(fic);

  if( (fic=fopen(rinf2_LUT_MOY,"r")) == NULL) {perror(rinf2_LUT_MOY); exit(-1);}
  fscanf(fic,"%f",&fpoub);
  for(i=0; i<rinf2_NE; i++)
    fscanf(fic,"%f",&rinf2_moy[i]);
  fscanf(fic,"%f",&fpoub);
  fscanf(fic,"%f",&rinf2_moy[rinf2_NES-1]);
  fclose(fic);
  if( (fic=fopen(rinf2_LUT_ECART,"r")) == NULL) {perror(rinf2_LUT_ECART); exit(-1);}
  fscanf(fic,"%f",&fpoub);
  for(i=0; i<rinf2_NE; i++)
    fscanf(fic,"%f",&rinf2_ecart[i]);
  fscanf(fic,"%f",&fpoub);
  fscanf(fic,"%f",&rinf2_ecart[rinf2_NES-1]);
  fclose(fic);

}

/* -------------------------------------------------------------------------------
 Calcul du Kd a partir des poids
 - Input:
  input[NE] = Rrs 443 490 510 555 Lambda
 ------------------------------------------------------------------------------- */
float rsup2_neuron_passe_avant(float input[rsup2_NE])
{
  float a[rsup2_NC1], y=0.0, x[rsup2_NE];
  int i,j;

  /* Normalisation */
  for(i=0; i<rsup2_NE; i++){
    x[i] = ((2./3.)*(input[i]-rsup2_moy[i]))/rsup2_ecart[i];
  }

  for(i=0;i<rsup2_NC1;i++){
    a[i] = 0.0;
    for(j=0;j<rsup2_NE;j++){
      a[i] += (x[j]*rsup2_w1[j][i]);
    }
    a[i] = 1.715905*(float)tanh((2./3.)*(double)(a[i] + rsup2_b1[i]));
  }

  for(j=0;j<rsup2_NC1;j++){
    y += (a[j]*rsup2_w2[j]);
  }

  /* Denormalisation */
  y = 1.5*(y + rsup2_b2)*rsup2_ecart[rsup2_NES-1] + rsup2_moy[rsup2_NES-1];
  y = (float)pow(10.,(double)y);
  return(y);
}

/* -------------------------------------------------------------------------------
 Calcul du Kd a partir des poids
 - Input:
  input[NE] = Rrs 443 490 510 555 670 Lambda
 ------------------------------------------------------------------------------- */
float rinf2_neuron_passe_avant(float input[rinf2_NE])
{
  float a[rinf2_NC1], y=0.0, x[rinf2_NE];
  int i,j;

  /* Normalisation */
  for(i=0; i<rinf2_NE; i++){
    x[i] = ((2./3.)*(input[i]-rinf2_moy[i]))/rinf2_ecart[i];
  }

  for(i=0;i<rinf2_NC1;i++){
    a[i] = 0.0;
    for(j=0;j<rinf2_NE;j++){
      a[i] += (x[j]*rinf2_w1[j][i]);
    }
    a[i] = 1.715905*(float)tanh((2./3.)*(double)(a[i] + rinf2_b1[i]));
  }

  for(j=0;j<rinf2_NC1;j++){
    y += (a[j]*rinf2_w2[j]);
  }

  /* Denormalisation */
  y = 1.5*(y + rinf2_b2)*rinf2_ecart[rinf2_NES-1] + rinf2_moy[rinf2_NES-1];
  y = (float)pow(10.,(double)y);
  return(y);
}


/*int main (int argc, char *argv[])
{
  FILE *fic;
  float data_in[rinf2_NE],result_in,result_out,res_norm, sumInf_rms=0., sumInf_rel=0., sumSup_rms=0., sumSup_rel=0.;
  int i,nb,lu;

  if( (fic=fopen("../LUTS/base_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_publi_091213_test.dat","r")) == NULL) {perror("input"); exit(-1);}
  neuron_lect_LUTs();

  nb = 0;
  while((lu=fscanf(fic,"%f",&data_in[0])) == 1){
    for(i=1; i<rinf2_NE; i++)
      fscanf(fic,"%f",&data_in[i]);
    fscanf(fic,"%f",&result_in);
    result_out = rinf2_neuron_passe_avant(data_in);
    result_in = (float)pow(10.,result_in);
    sumInf_rms += (float)pow((double)(result_in-result_out),2.);
    sumInf_rel +=  (float)sqrt((double)(((result_in-result_out)/result_in) * ((result_in-result_out)/result_in)));
    printf("%f %f",result_in,result_out);

    data_in[5] = data_in[6];
    result_out = rsup2_neuron_passe_avant(data_in);
    sumSup_rms += (float)pow((double)(result_in-result_out),2.);
    sumSup_rel +=  (float)sqrt((double)(((result_in-result_out)/result_in) * ((result_in-result_out)/result_in)));
    printf(" %f\n",result_out);
    nb++;
  }
  printf("rmseInf = %f, rel errInf= %f,  rmseSup = %f, rel errSup= %f  nb = %d\n",(float)sqrt((double)sumInf_rms/nb),sumInf_rel/nb, (float)sqrt((double)sumSup_rms/nb),sumSup_rel/nb, nb);



  fclose(fic);
  exit(1);
}*/

