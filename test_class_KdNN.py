#!/usr/bin/python
import class_KdJamet as kdj
import numpy as np
import sys
if len(sys.argv) != 4:
  print "test_class_KdNN.py must be launch with three arguments:"
  print " $ test_class_KdNN.py LOG_KdNeuralNetwork_TestCase_Input.csv your_output.csv idSensor[idMERIS, idSEAWIFS, idMODIS]"
  sys.exit(-1)

# class KdJamet instance initialization for seleted sensor (argv[3])
K = kdj.KdJamet(sys.argv[3])
# test case Rrs file (argv[1]) reading  (ie: LOG_KdNeuralNetwork_TestCase_Input.csv)
tab = np.loadtxt(sys.argv[1], skiprows=1)

# array to store Kd results 
KDs = np.zeros(tab.shape,dtype=np.float32)
# for each line of input file compute the Kd for selected Sensor
for i in range(tab.shape[0]):
  KDs[i,:] = K.compute_allKd(tab[i,:])
# write result in argv[2] file
np.savetxt(sys.argv[2],KDs,fmt='%.7f',header="Kd%d Kd%d Kd%d Kd%d Kd%d Kd%d" %(K.WL[0],K.WL[1],K.WL[2],K.WL[3],K.WL[4],K.WL[5]))
  

