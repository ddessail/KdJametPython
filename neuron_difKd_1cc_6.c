/*
 Kd estimation references :
 Jamet, C., H. Loisel, and D. Dessailly (2012), Retrieval of the spectral diffuse attenuation coefficient Kd(l) in open and coastal ocean waters using a neural network inversion, J. Geophys. Res., 117, C10023, doi:10.1029/2012JC008076.
 Jamet, C., H., Loisel and D., Dessailly, 2010. Estimation of the diffuse attenuation coefficient Kd(lambda) with a neural network,  Proceedings of IGARSS '11
 Jamet, C., H., Loisel and D., Dessailly, 2010. Empirical nonlinear determination of the diffuse attenuation coefficient Kd(490) in coastal waters from ocean color image,  Proceedings of SPIE-Asia-Pacific Remote Sensing, DOI: 10.1117/12.869730

 C coding version :
 2012-03-15 D. Dessailly david.dessailly@univ-littoral.fr

 Laboratoire d'Oceanoligie et Geoscience (LOG)
*/

#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include "neuron_difKd1cc_6.h"

/* -------------------------------------------------------------------------------
  Lecture des differentes Look Up Tables  -  LUTs reading
   - Files names and path define in neuron1cc.h
 -------------------------------------------------------------------------------  */
void neuron_lect_LUTs_dif(char LUT_PATH[])
{
  FILE *fic;
  int i,j,poub;
  char *ligne=malloc(sizeof(char)*150), nomfic[500];
  float fpoub;

  sprintf(nomfic,"%s/%s",LUT_PATH,dif_LUT_POIDS);
  if( (fic=fopen(nomfic,"r")) == NULL) {perror(nomfic); exit(-1);}
  fgets(ligne,150,fic);
  for(i=0; i<DNC1; i++)
    fscanf(fic,"%d %d %f",&poub,&poub,&dif_b1[i]);
  fscanf(fic,"%d %d %f",&poub,&poub,&dif_b2);
  for(j=0; j<DNE; j++){
   for(i=0; i<DNC1; i++)
     fscanf(fic,"%d %d %f",&poub,&poub,&dif_w1[j][i]);
  }
  for(j=0; j<DNC1; j++)
    fscanf(fic,"%d %d %f",&poub,&poub,&dif_w2[j]);
  fclose(fic);

  sprintf(nomfic,"%s/%s",LUT_PATH,dif_LUT_MOY);
  if( (fic=fopen(nomfic,"r")) == NULL) {perror(nomfic); exit(-1);}
  fscanf(fic,"%f",&fpoub);
  for(i=0; i<DNE; i++)
    fscanf(fic,"%f",&dif_moy[i]);
  for(i=0; i<3; i++)
    fscanf(fic,"%f",&fpoub);
  fscanf(fic,"%f",&dif_moy[DNES-1]);
  fclose(fic);
  sprintf(nomfic,"%s/%s",LUT_PATH,dif_LUT_ECART);
  if( (fic=fopen(nomfic,"r")) == NULL) {perror(nomfic); exit(-1);}
  fscanf(fic,"%f",&fpoub);
  for(i=0; i<DNE; i++)
    fscanf(fic,"%f",&dif_ecart[i]);
  for(i=0; i<3; i++)
    fscanf(fic,"%f",&fpoub);
  fscanf(fic,"%f",&dif_ecart[DNES-1]);
  fclose(fic);
}

/* -------------------------------------------------------------------------------
 Calcul du Kd a partir des poids  -  Kd computing from NN weights
 - Input:
  input[DNE] = Rrs[412 443 490 510 555], WaveLenght (for desired output Kd)
 ------------------------------------------------------------------------------- */
float neuron_passe_avant_dif(float input[DNE])
{
  float a[DNC1], y=0.0, x[DNE];
  int i,j;

  /* Normalisation */
  for(i=0; i<DNE; i++){
    x[i] = ((2./3.)*(input[i]-dif_moy[i]))/dif_ecart[i];
  }

  for(i=0;i<DNC1;i++){
    a[i] = 0.0;
    for(j=0;j<DNE;j++){
      a[i] += (x[j]*dif_w1[j][i]);
    }
    a[i] = 1.715905*(float)tanh((2./3.)*(double)(a[i] + dif_b1[i]));
  }

  for(j=0;j<DNC1;j++){
    y += (a[j] * dif_w2[j]);
  }

  /* Denormalisation */
  y = 1.5*(y + dif_b2)*dif_ecart[DNES-1] + dif_moy[DNES-1];
  y = (float)pow(10.,(double)y);
  return(y);
}
