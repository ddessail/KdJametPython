#include <stdio.h>

/* nombre de couches cachees - hidden layer number */
#define DNC 1
/* nombre de donnees d entree - input data number*/
#define DNE 5
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define DNC1 6
/* nombre de donnees de sortie - nb output*/
#define DNS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define DNES 6

/* LUTs file names */
#define dif_LUT_POIDS "KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_ascii_6x1_hh_6_publi_150114.sn"
#define dif_LUT_MOY "Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_publi_150114.dat"
#define dif_LUT_ECART "Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_publi_150114.dat"

/* LUTs parameters definition */
float dif_b1[DNC1], dif_b2;
float dif_w1[DNE][DNC1], dif_w2[DNC1];
float dif_moy[DNES], dif_ecart[DNES];
