/*
  Short code to test Jamet C. Neural Network Kd inversion.

  C coding version :
  2012-03-15 D. Dessailly david.dessailly@univ-littoral.fr

  LOG (Laboratoire d'Oceanoligie et Geoscience)
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <memory.h>
#include <math.h>
#include "iop_Rrs_neuron.h"
#include "neuron_switch160715.h"

#define PI 3.14159265
#define NBWL 6
#define NBNE 7

/* neuron_kd_1cc.c functions*/
extern void neuron_lect_LUTswitch(int idCap, char *path);
extern float rsupOL_neuron_passe_avant(float x[rsupOL_NE+1]); /* Neural network Kd computation switch SeaWiFS */
extern float rinfOL_neuron_passe_avant(float x[rinfOL_NE]);
// extern float rsupMO_neuron_passe_avant(float x[rsupMO_NE+1]); /* Neural network Kd computation switch MODIS */
// extern float rinfMO_neuron_passe_avant(float x[rinfMO_NE]);
// extern float rsupOL_neuron_passe_avant(float x[rsupOL_NE+1]);  /* Neural network Kd computation switch MERIS / OLCI */
// extern float rinfOL_neuron_passe_avant(float input[rinfOL_NE]);
//
float LO[NBWL] = {413.,443.,490.,510.,560.,665.};

int main (argc, argv)
int argc;
char *argv[];
{
  FILE *fic, *out;
  int i, status;
  char nomfic[500];

  float RRS[NBNE];  /* inputs */
  float KD[NBWL]; /* outputs */
  float s2RRS[rinfOL_NE], rap;

  if(argc != 3){
    printf("launch with arguments : \n");
    printf("  - input file name [file with %d columns for SeaWiFS Wave-length : 412.,443.,490.,510.,550.,670.]\n",(int)NBWL);
    printf("  - output filename\n");
  }

  /*## Kd LUTs initialization ##*/
  neuron_lect_LUTswitch(idMERIS, "../LUTS");

  /* input file */
  if( (fic=fopen(argv[1],"r")) == NULL ) {perror(argv[1]); exit(-1);}
  fgets(nomfic,500,fic);
  /* foutput file */
  if( (out=fopen(argv[2],"w")) == NULL ) {perror(argv[3]); exit(-1);}
  for(i=0; i<NBLO; i++)
    fprintf(out,"Kd%g ",LO[i]);
  fprintf(out,"\n");

  while( (status = fscanf(fic,"%f",&RRS[0])) == 1){
    for(i=1; i<NBWL; i++)
      fscanf(fic,"%f",&RRS[i]);
    /* Choix du reseau a utiliser en fonction du rapport 490/555 */
    rap = RRS[2]/RRS[4];
    if( rap >= .85 )
      for(i=0; i<rsupOL_NE-1; i++)
        s2RRS[i] = RRS[i+1];
    else
      for(i=0; i<rinfOL_NE-1; i++)
        s2RRS[i] = RRS[i+1];

    for(i=0; i<NBWL; i++){
      if( rap >= .85 ){
        s2RRS[rsupOL_NE-1] = LO[i];
        KD[i] = rsupOL_neuron_passe_avant(s2RRS);
      }
      else{
        s2RRS[rinfOL_NE-1] = LO[i];
        KD[i] = rinfOL_neuron_passe_avant(s2RRS);
      }
    }

    for(i=0; i<NBLO; i++)
      fprintf(out,"%g ", KD[i]);
    fprintf(out,"\b\n");
  }

  fclose(fic);
  fclose(out);
  return(0);
}
