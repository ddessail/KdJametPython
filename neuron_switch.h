#include <stdio.h>

/* Definition  des variables pour le reseau avec Rrs490/Rrs555 > 1.5 */
/* nombre de couches cachees - hidden layer number */
#define rsup_NC 2
/* nombre de donnees d entree - input data number*/
#define rsup_NE 6
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define rsup_NC1 6
/* nombre de neurones de la deuxieme couche cachee */
#define rsup_NC2 6
/* nombre de donnees de sortie - nb output*/
#define rsup_NS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define rsup_NES 7

/* LUTs file names */
#define rsup_LUT_POIDS "../LUTS/KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_555_log_Kd_lambda_merge_seuil_3_ascii_6x1_hh_6_6_publi_091213.sn"
#define rsup_LUT_MOY "../LUTS/Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_555_log_Kd_lambda_merge_seuil_3_publi_091213.dat"
#define rsup_LUT_ECART "../LUTS/Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_555_log_Kd_lambda_merge_seuil_3_publi_091213.dat"


/* Definition  des variables pour le reseau avec Rrs490/Rrs555 <= 1.5 */
/* nombre de couches cachees - hidden layer number */
#define rinf_NC 2
/* nombre de donnees d entree - input data number*/
#define rinf_NE 7
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define rinf_NC1 4
/* nombre de neurones de la deuxieme couche cachee */
#define rinf_NC2 4
/* nombre de donnees de sortie - nb output*/
#define rinf_NS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define rinf_NES 8

/* LUTs file names */
#define rinf_LUT_POIDS "../LUTS/KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_ascii_6x1_hh_4_4_publi_091213.sn"
#define rinf_LUT_MOY "../LUTS/Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_publi_091213.dat"
#define rinf_LUT_ECART "../LUTS/Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_412_to_670_log_Kd_lambda_merge_seuil_15_publi_091213.dat"

/* LUTs parameters definition */
float rsup_b1[rsup_NC1], rsup_b2[rsup_NC2], rsup_b3;
float rsup_w1[rsup_NE][rsup_NC1], rsup_w2[rsup_NC1][rsup_NC2], rsup_w3[rsup_NC2];

float rsup_moy[rsup_NES], rsup_ecart[rsup_NES];

float rinf_b1[rinf_NC1], rinf_b2[rinf_NC2], rinf_b3;
float rinf_w1[rinf_NE][rinf_NC1], rinf_w2[rinf_NC1][rinf_NC2], rinf_w3[rinf_NC2];
float rinf_moy[rinf_NES], rinf_ecart[rinf_NES];