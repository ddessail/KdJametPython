#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include "neuron2cc.h"

/* -------------------------------------------------------------------------------
 Lecture des differrentes Look Up Tables
 -------------------------------------------------------------------------------  */
void neuron_lect_LUTs()
{
  FILE *fic;
  int i,j,poub;
  char *ligne=malloc(sizeof(char)*150);
  float fpoub;

  if( (fic=fopen(LUT_POIDS,"r")) == NULL) {perror(LUT_POIDS); exit(-1);}
  fgets(ligne,150,fic);
  for(i=0; i<NC1; i++)
    fscanf(fic,"%d %d %f",&poub,&poub,&b1[i]);
  for(i=0; i<NC2; i++)
    fscanf(fic,"%d %d %f",&poub,&poub,&b2[i]);
  fscanf(fic,"%d %d %f",&poub,&poub,&b3);
  for(j=0; j<NE; j++){
   for(i=0; i<NC1; i++)
     fscanf(fic,"%d %d %f",&poub,&poub,&w1[j][i]);
  }
  for(j=0; j<NC1; j++){
    for(i=0; i<NC2; i++)
      fscanf(fic,"%d %d %f",&poub,&poub,&w2[j][i]);
  }
  for(i=0; i<NC2; i++)
    fscanf(fic,"%d %d %f",&poub,&poub,&w3[i]);
  fclose(fic);

  if( (fic=fopen(LUT_MOY,"r")) == NULL) {perror(LUT_MOY); exit(-1);}
  fscanf(fic,"%f",&fpoub);
  for(i=0; i<NES; i++)
    fscanf(fic,"%f",&moy[i]);
  fclose(fic);
  if( (fic=fopen(LUT_ECART,"r")) == NULL) {perror(LUT_ECART); exit(-1);}
  fscanf(fic,"%f",&fpoub);
  for(i=0; i<NES; i++)
    fscanf(fic,"%f",&ecart[i]);
  fclose(fic);
}

/* -------------------------------------------------------------------------------
 Calcul du Kd a partir des poids
 - Input:
  input[NE] = Rrs (412) 443 490 510 555 670 ASOL
 ------------------------------------------------------------------------------- */
float neuron_passe_avant(float input[NE+1])
{
  float a[NC1], b[NC2], y=0.0, x[NE];
  int i,j;

  /* Normalisation */
  for(i=1; i<=NE; i++){
    x[i-1] = ((2./3.)*(input[i]-moy[i-1]))/ecart[i-1];
  }

  for(i=0;i<NC1;i++){
    a[i] = 0.0;
    for(j=0;j<NE;j++){
      a[i] += (x[j]*w1[j][i]);
    }
    a[i] = 1.715905*(float)tanh((2./3.)*(double)(a[i] + b1[i]));
  }
  for(i=0;i<NC2;i++){
    b[i] = 0.0;
    for(j=0;j<NC1;j++){
      b[i] += (a[j]*w2[j][i]);
    }
    b[i] = 1.715905*(float)tanh((2./3.)*(double)(b[i] + b2[i]));
  }
  for(j=0;j<NC2;j++){
    y += (b[j]*w3[j]);
  }

  /* Denormalisation */
  y = 1.5*(y + b3)*ecart[NES-1] + moy[NES-1];
  y = (float)pow(10.,y);
  return(y);
}


/*
int main (int argc, char *argv[])
{
  FILE *fic;
  float data_in[NE],result_in,result_out,res_norm, sum_rms=0., sum_rel=0.;
  int i,nb,lu;

  if( (fic=fopen("LUTS/base_Kd490_IOCCG_NOMAD_all_log_Kd_test.dat","r")) == NULL) {perror("input"); exit(-1);}
  neuron_lect_LUTs();

  nb = 0;
  while((lu=fscanf(fic,"%f",&data_in[0])) == 1){
    for(i=1; i<NE; i++)
      fscanf(fic,"%f",&data_in[i]);
    fscanf(fic,"%f",&result_in);
    res_norm = ((2./3.)*(result_in-moy[NES-1]))/ecart[NES-1];
    result_out = passe_avant(data_in);
    result_in = 1.5*res_norm*ecart[NES-1] + moy[NES-1];
    result_out = (float)pow(10.,result_out);
    result_in = (float)pow(10.,result_in);

    sum_rms += (float)pow((double)(result_in-result_out),2.);
    sum_rel +=  (float)sqrt((double)(((result_in-result_out)/result_in) * ((result_in-result_out)/result_in)));
    printf("%f %f\n",result_in,result_out);
    nb++;
  }
  printf("rmse = %f, rel err= %f   nb = %d\n",(float)sqrt((double)sum_rms/nb),sum_rel/nb,nb);
  fclose(fic);
  exit(1);
}*/
