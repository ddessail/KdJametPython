#include <stdio.h>

/* nombre de couches cachees - hidden layer number */
#define DNC 1
/* nombre de donnees d entree - input data number*/
#define DNE 5
/* nombre de neurones de la premiere couche cachee - number of neuron in the first hidden layer */
#define DNC1 26
/* nombre de donnees de sortie - nb output*/
#define DNS 1
/* nombre de neurones d'entree + sortie - nb input + nb output*/
#define DNES 6

/* LUTs file names */
#define LUT_POIDS "../LUTS/KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_ascii_6x1_hh_26_publi_150114.sn"
#define LUT_MOY "../LUTS/Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_publi_150114.dat"
#define LUT_ECART "../LUTS/Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_publi_150114.dat"

/* LUTs parameters definition */
float b1[DNC1], b2;
float w1[DNE][DNC1], w2[DNC1];
float moy[DNES], ecart[DNES];
