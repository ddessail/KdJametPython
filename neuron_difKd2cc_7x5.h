#include <stdio.h>

/* nombre de couches cachees */
#define DNC 2
/* nombre de donnees d entree*/
#define DNE 5
/* nombre de neurones de la premiere couche cachee */
#define DNC1 7
/* nombre de neurones de la deuxieme couche cachee */
#define DNC2 5
/* nombre de donnees de sortie */
#define DNS 1
/* nombre de neurones d'entree + sortie  */
#define DNES 6

#define LUT_POIDS "../LUTS/KdSeaWiFS_poids_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_ascii_6x1_hh_7_5_publi_150114.sn"
#define LUT_MOY "../LUTS/Moy_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_publi_150114.dat"
#define LUT_ECART "../LUTS/Ecart_KdSeaWiFS_IOCCG_NOMAD_BOUM_ss_12_COASTCOLOUR_443_to_670_Kd_lambda_merge_seuil_15_diff_publi_150114.dat"

float b1[DNC1], b2[DNC2], b3;
float w1[DNE][DNC1], w2[DNC1][DNC2], w3[DNC2];
float moy[DNES], ecart[DNES];
