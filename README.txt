
 C coding version for Estimation of the diffuse attenuation coefficient Kd(lambda) with a neural network.

- LOG_KdNeuralNetwork_test.c
 A small Test case application to validate neuron_kd_switch3.c functions.  You can compile it using : comp_LOG_KdNeuralNetwork_test.sh
 Must be launch with two arguments, this way :
 $ LOG_KdNeuralNetwork_test LOG_KdNeuralNetwork_TestCase_Input.csv your_output.csv

 Result are in your_output.csv, which may match LOG_KdNeuralNetwork_TestCase_Output.csv if everithing is OK!

- neuron_kd_switch3.c include three functions :
 + neuron_lect_LUTswitch3()
  Read the LUTs used by rinf3_neuron_passe_avant() and rsup3_neuron_passe_avant
  !!! path to the LUTs files are defined in neuron_switch3.h file, be sure to adapt them to your configuration !!!
 + float rinf3_neuron_passe_avant(float input[NE])
  Compute Kd from Neural network weigths if [Rrs490/Rrs55 < .85]
  input must be initialized with Rrs(443 490 510 555 670) and the Wave-length(nm) at which Kd had to be computed.
  Kd is returned
 + float rsup3_neuron_passe_avant(float input[NE])
  Compute Kd from Neural network weigths if [Rrs490/Rrs55 >= .85]
  input must be initialized with Rrs(443 490 510 555) and the Wave-length(nm) at which Kd had to be computed.
  Kd is returned
