/*
 Kd estimation references :
 Jamet, C., H. Loisel, and D. Dessailly (2012), Retrieval of the spectral diffuse attenuation coefficient Kd(l) in open and coastal ocean waters using a neural network inversion, J. Geophys. Res., 117, C10023, doi:10.1029/2012JC008076.
 Jamet, C., H., Loisel and D., Dessailly, 2010. Estimation of the diffuse attenuation coefficient Kd(lambda) with a neural network,  Proceedings of IGARSS '11
 Jamet, C., H., Loisel and D., Dessailly, 2010. Empirical nonlinear determination of the diffuse attenuation coefficient Kd(490) in coastal waters from ocean color image,  Proceedings of SPIE-Asia-Pacific Remote Sensing, DOI: 10.1117/12.869730

 C coding version :
 2012-03-15 D. Dessailly david.dessailly@univ-littoral.fr

 Laboratoire d'Oceanoligie et Geoscience (LOG)
*/

#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include "neuron1cc.h"

/* -------------------------------------------------------------------------------
  Lecture des differentes Look Up Tables  -  LUTs reading
   - Files names and path define in neuron1cc.h
 -------------------------------------------------------------------------------  */
void neuron_lect_LUTs()
{
  FILE *fic;
  int i,j,poub;
  char *ligne=malloc(sizeof(char)*150);
  float fpoub;

  if( (fic=fopen(LUT_POIDS,"r")) == NULL) {perror(LUT_POIDS); exit(-1);}
  fgets(ligne,150,fic);
  for(i=0; i<NC1; i++)
    fscanf(fic,"%d %d %f",&poub,&poub,&b1[i]);
  fscanf(fic,"%d %d %f",&poub,&poub,&b2);
  for(j=0; j<NE; j++){
   for(i=0; i<NC1; i++)
     fscanf(fic,"%d %d %f",&poub,&poub,&w1[j][i]);
  }
  for(j=0; j<NC1; j++)
    fscanf(fic,"%d %d %f",&poub,&poub,&w2[j]);
  fclose(fic);

  if( (fic=fopen(LUT_MOY,"r")) == NULL) {perror(LUT_MOY); exit(-1);}
  for(i=0; i<NES; i++)
    fscanf(fic,"%f",&moy[i]);
  fclose(fic);
  if( (fic=fopen(LUT_ECART,"r")) == NULL) {perror(LUT_ECART); exit(-1);}
  for(i=0; i<NES; i++)
    fscanf(fic,"%f",&ecart[i]);
  fclose(fic);
}

/* -------------------------------------------------------------------------------
 Calcul du Kd a partir des poids  -  Kd computing from NN weights
 - Input:
  input[NE] = Rrs[412 443 490 510 555 670], WaveLenght (for desired output Kd)
 ------------------------------------------------------------------------------- */
float neuron_passe_avant(float input[NE])
{
  float a[NC1], y=0.0, x[NE];
  int i,j;

  /* Normalisation */
  for(i=0; i<NE; i++){
    x[i] = ((2./3.)*(input[i]-moy[i]))/ecart[i];
  }

  for(i=0;i<NC1;i++){
    a[i] = 0.0;
    for(j=0;j<NE;j++){
      a[i] += (x[j]*w1[j][i]);
    }
    a[i] = 1.715905*(float)tanh((2./3.)*(double)(a[i] + b1[i]));
  }

  for(j=0;j<NC1;j++){
    y += (a[j] * w2[j]);
  }

  /* Denormalisation */
  y = 1.5*(y + b2)*ecart[NES-1] + moy[NES-1];
  y = (float)pow(10.,(double)y);
  return(y);
}
